﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_set_skate_default_location : MonoBehaviour
{
    [SerializeField] Vector2[] default_angle_choice_location;// Needed to make the answer cards go back to its starting location 
    [SerializeField] Vector2[] default_blue_angle_choice_location;
    [SerializeField] scr_card_manager ref_card_manager;
    //const float y_default_value = 120.0f;
    //public float[] x_default_value;
    //[SerializeField] scr_blue_screen ref_blue_manager;

    void Awake()
    {
        // Tentative to be relocated for blue location
        default_blue_angle_choice_location  = new Vector2[4];
    }

    void Start()
    {
        Set_Skate_Card_Default_Location();
    }

    void Set_Skate_Card_Default_Location()
    {
        // Tentative to be relocated for default location
        default_angle_choice_location = new Vector2[4];

        // Get the default location of ref_Angle_choice_cards
        for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {
            default_angle_choice_location[i] = ref_card_manager.Prop_Angle_Choice_Cards[i].
            GetComponent<RectTransform>().anchoredPosition;
        }


    }
    public Vector2[] Prop_Default_Angle_Choice_Location 
    {
        get{return default_angle_choice_location;}
        set{default_angle_choice_location = value;}
    }
    public Vector2[] Prop_Default_Blue_Angle_Choice_Location 
    {
        get{return default_blue_angle_choice_location;}
        set{default_blue_angle_choice_location = value;}
    }
}
