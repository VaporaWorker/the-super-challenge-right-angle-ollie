﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_scale_perma : MonoBehaviour
{
    [SerializeField]GameObject ref_gameObject;
    [SerializeField]Vector3 scale_goal;
    [SerializeField]float duration;

    void OnEnable()
    {
        Scale_Tween();
    }

    public void Scale_Tween()
    {
        LeanTween.scale(ref_gameObject,scale_goal,duration);
    }

    void OnDisable()
    {
        ref_gameObject.SetActive(false);
    }
}
