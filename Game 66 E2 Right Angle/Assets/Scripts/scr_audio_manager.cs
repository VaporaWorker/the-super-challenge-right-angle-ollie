﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class scr_audio_manager : MonoBehaviour
{   
    [SerializeField] scrobj_sound_template[] audio_details;

    public static scr_audio_manager instance;
    void Awake()
    {
        foreach(scrobj_sound_template scrobj_sound in audio_details)
        {
            scrobj_sound.aud_source = gameObject.AddComponent<AudioSource>();
            scrobj_sound.aud_source.clip = scrobj_sound.clip;
            scrobj_sound.aud_source.volume = scrobj_sound.volume;
            scrobj_sound.aud_source.loop = scrobj_sound.loop;
            scrobj_sound.aud_source.playOnAwake = scrobj_sound.awake;
        }
        if(instance == null)
        {
            instance = this;
        }
        else
        {   
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(instance);
    }
    void Start()
    {
        
    }


    public void Play_Audio(String name)
    {
        
        scrobj_sound_template scrobj_st = Array.Find(audio_details,audio_detail => audio_detail.name == name);
        if(scrobj_st == null)
        {
            Debug.LogWarning("Sound: "+ name + "not found");
            return;
        } 
        scrobj_st.aud_source.Play();
        Debug.Log("Audio: " + name);
    }

    public void Pause_Audio(String name)
    {
        scrobj_sound_template scrobj_st = Array.Find(audio_details,audio_detail => audio_detail.name == name);
        scrobj_st.aud_source.Pause();
    }

    public void Stop_Audio(String name)
    {
        scrobj_sound_template scrobj_st = Array.Find(audio_details,audio_detail => audio_detail.name == name);
        scrobj_st.aud_source.Stop();
        
    }

}
