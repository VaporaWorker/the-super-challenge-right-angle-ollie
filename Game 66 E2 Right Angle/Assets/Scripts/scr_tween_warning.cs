﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_tween_warning : MonoBehaviour
{
    
    [SerializeField]Vector3 max_default_size;
    [SerializeField]Vector3 smallest_size;
    [SerializeField]Vector3 beat_size;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField] float duration;
    [SerializeField] float delay;
    void Awake()
    {
        //ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    public void Enlarge_Warning_Tween()
    {
        LeanTween.scale(this.gameObject, max_default_size,duration).setOnComplete(Warning_Beat);
    }
    public void Warning_Beat()
    {
        LeanTween.scale(this.gameObject, beat_size,duration).setLoopPingPong(6);
        No_Guessing_Audio();

    }

    public void No_Guessing_Audio()
    {
        //ref_audio_manager.Play_Audio("no_gsng_appers");
        ref_audio_manager_2.Play_Audio("no_guessing_sfx");
        StartCoroutine(Danger_VO());
    }

    public IEnumerator Danger_VO()
    {
        yield return new WaitForSeconds(delay-1.0f);
        //ref_audio_manager.Play_Audio("after_no_gsing_appers");
        ref_audio_manager_2.Play_Audio("danger_vo_sfx");
        yield return new WaitForSeconds(delay);
        Fade_Object();
    }   

    void Fade_Object()
    {
        LeanTween.alpha(this.gameObject.GetComponent<RectTransform>(),0.0f,duration).setOnComplete(Disable_Object);
    }

    void Disable_Object()
    {
        this.gameObject.SetActive(false);
        LeanTween.scale(this.gameObject, smallest_size,0);
        LeanTween.alpha(this.gameObject.GetComponent<RectTransform>(),1.0f,0);
        //this.gameObject.GetComponent<Image>().enabled = false;  
    }
}
