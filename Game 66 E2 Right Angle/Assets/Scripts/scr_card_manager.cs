﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_card_manager : MonoBehaviour
{
    [SerializeField]Image ref_Question_card;// The Card that gives out the question
    [SerializeField]scr_blue_screen ref_Blue_screen;
    [SerializeField]GameObject[] array_cards;// An array of Skateboard Cards
    [SerializeField]Sprite[] array_sprite_cards;
    [SerializeField]GameObject[] angle_choice_cards;// Needed for te answer cards to change info dynamically
    [SerializeField]scrobj_blue_screen_template[] blue_questionares;// scriptable objects to dynamically change the 4 answer cards info
    //[SerializeField]GameObject blue_screen;

    void Awake()
    {
       
    }

    void Update()
    {

    }

    public void Set_Array_Card_Buttons_Bool(bool condition)
    {
        for(int i = 0;i < Prop_Array_Cards.Length;i++)
        {   // Set all Array Cards buttons to false
            Prop_Array_Cards[i].GetComponent<Button>().enabled = condition;
        }
    }
    public GameObject[] Prop_Array_Cards
    {
        get{return array_cards;}
        set{array_cards = value;}
    }

    public Sprite[] Prop_Array_Image_Cards
    {
        get{return array_sprite_cards;}
        set{array_sprite_cards = value;}
    }

    public GameObject[] Prop_Angle_Choice_Cards
    {
        get{return angle_choice_cards;}
        set{angle_choice_cards = value;}
    }

    public Image Prop_Question_Card
    {
        get{return ref_Question_card;}
        set{ref_Question_card = value;}
    }

    public scrobj_blue_screen_template[] Prop_Blue_Questionares
    {
        get{return blue_questionares;}
        set{blue_questionares = value;}
    }
}
