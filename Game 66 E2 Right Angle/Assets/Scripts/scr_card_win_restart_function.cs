﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scr_card_win_restart_function : MonoBehaviour
{
    [SerializeField]scr_blue_screen ref_Blue_screen;// needed for the boolean value of time blue screen
    [SerializeField]scr_info_button_function ref_info_button_function;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]scr_guess_condition ref_guess_condition;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_vfx_manager ref_vfx_manager;
    public GameObject warning_sign;// Gameobject to store the warning no guessing pop up
    public GameObject x_mark_dog_screen;// Gameobject that sores the X mark for wrong Answers
    public GameObject super_logo;
    public GameObject genius_image;
    public GameObject super_image;
    public GameObject challenge_object;
    public GameObject completed_object;

    void Awake()
    {
        ref_guess_condition = this.GetComponent<scr_guess_condition>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>(); 
    }

    void Start()
    {   
        if(ref_guess_condition.Prop_Num_Answered_Correctly >= 12)
        {
            StartCoroutine(Win_Phase());
        }
    } 
    void OnEnable()
    {   
        //Debug.Log("On Enable");
        for(int i = 0;i < ref_card_manager.Prop_Array_Cards.Length; i++)
        {
            ref_card_manager.Prop_Array_Cards[i].GetComponent<Image>().sprite =
            ref_card_manager.Prop_Array_Image_Cards[i];
            
        }
        //ref_audio_manager.Stop_Audio("bg_audio");
        ref_audio_manager_2.Stop_Audio("bg_audio_sfx");
        ref_audio_manager_2.Prop_Toggle_Music = false;
        ref_info_button_function.x_mark_toggle_music.SetActive(true);
    }

    IEnumerator Win_Phase()
    {   
        // Disable Button UI
        ref_info_button_function.Prop_How_to_Play_button_object.SetActive(false);
        ref_info_button_function.Prop_Clue_button_object.SetActive(false);
        ref_info_button_function.Prop_Teacher_button_object.SetActive(false);
        ref_info_button_function.Prop_Is_Complete_Level = true;
        // Play Audio
        //ref_audio_manager.Play_Audio("all_cards_done_applause");
        ref_audio_manager_2.Play_Audio("applause_sfx");
        yield return new WaitForSeconds(2.5f);

        //ref_audio_manager.Play_Audio("after_applause_unbelievable");
        ref_audio_manager_2.Play_Audio("unbelievable_sfx");

        yield return new WaitForSeconds(1.2f);

        // Enable Logo and You Genius Text
        super_logo.SetActive(true);
        //ref_audio_manager.Play_Audio("stamp_sfx");
        ref_audio_manager_2.Play_Audio("rubber_sfx");

        genius_image.SetActive(true);

        //ref_audio_manager.Play_Audio("stamp_sfx");
        ref_audio_manager_2.Play_Audio("rubber_sfx");

        yield return new WaitForSeconds(1.25f);

        // Enable Super Image
        super_image.SetActive(true);
        //ref_audio_manager.Play_Audio("stamp_sfx");
        ref_audio_manager_2.Play_Audio("rubber_sfx");

        yield return new WaitForSeconds(0.45f);
        // Play Audio
        //ref_audio_manager.Play_Audio("super_genius");
        ref_audio_manager_2.Play_Audio("pre_super_genius_sfx");
        yield return new WaitForSeconds(1.0f);
        // Play Audio
        //ref_audio_manager.Play_Audio("super_genius_vo");
        ref_audio_manager_2.Play_Audio("super_genius_sfx");
        yield return new WaitForSeconds(2.0f);

        //ref_audio_manager.Play_Audio("congratulations");
        //ref_audio_manager_2.Play_Audio("congrats_sfx");

        //yield return new WaitForSeconds(0.25f);
        ref_audio_manager_2.Play_Audio("reward_cards_sfx");
        // Set all cards to be true 
        for(int i = 0; i < ref_card_manager.Prop_Array_Cards.Length; i++)
        {       
            yield return new WaitForSeconds(0.22f);
            
            ref_card_manager.Prop_Array_Cards[i].SetActive(true);
            ref_card_manager.Prop_Array_Cards[i].GetComponent<Button>().enabled = false;
        }

        yield return new WaitForSeconds(0.5f);

        for(int i = 0; i < ref_card_manager.Prop_Array_Cards.Length; i++)
        {
            yield return new WaitForSeconds(0.20f);
            // Set Array of Explosions position to the position of Array Cards
            ref_vfx_manager.Prop_Array_Explosions[i].GetComponent<RectTransform>().anchoredPosition = 
            ref_card_manager.Prop_Array_Cards[i].GetComponent<RectTransform>().anchoredPosition;

            //ref_audio_manager.Play_Audio("explosion_sfx");
            ref_audio_manager_2.Play_Audio("explo_sfx");
            ref_vfx_manager.Prop_Array_Explosions[i].GetComponent<scr_animtr_explosion>().Animation_Begin();
            
            ref_card_manager.Prop_Array_Cards[i].SetActive(false);

        }
        yield return new WaitForSeconds(0.25f);
        challenge_object.SetActive(true);
        //ref_audio_manager.Play_Audio("stamp_sfx");
        ref_audio_manager_2.Play_Audio("rubber_sfx");
        yield return new WaitForSeconds(1.25f);
        completed_object.SetActive(true);
        //ref_audio_manager.Play_Audio("stamp_sfx");
        ref_audio_manager_2.Play_Audio("rubber_sfx");

        yield return new WaitForSeconds(1.0f);
        
        //ref_audio_manager.Play_Audio("after_card_explode_you_did_it");
        ref_audio_manager_2.Play_Audio("you_did_it_sfx");
        yield return new WaitForSeconds(0.75f);

        //ref_audio_manager.Play_Audio("after_you_did_it");
        ref_audio_manager_2.Play_Audio("woo_yeah_sfx");
        yield return new WaitForSeconds(0.75f);

        //ref_audio_manager.Play_Audio("bg_play_every_pg");
        ref_audio_manager_2.Play_Audio("bg_audio_sfx");
        ref_audio_manager_2.Prop_Toggle_Music = true;
        ref_info_button_function.x_mark_toggle_music.SetActive(false);
    }
}
