﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_restart : MonoBehaviour
{
    [SerializeField]float duration;
    [SerializeField]Vector3 scale_goal;
    [SerializeField]Vector3 default_scale;

    public void Restart_Size_Tween()
    {
        LeanTween.scale(this.gameObject,scale_goal,duration);
    }

    void OnEnable()
    {
        Restart_Size_Tween();
    }

    void OnDisable()
    {
        this.gameObject.GetComponent<RectTransform>().localScale = default_scale;
    }

}
