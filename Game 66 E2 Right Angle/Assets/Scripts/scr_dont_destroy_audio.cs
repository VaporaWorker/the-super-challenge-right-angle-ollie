﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_dont_destroy_audio : MonoBehaviour
{
    [SerializeField]static scr_dont_destroy_audio ref_dont_destroy_audio;
    public AudioClip audio_clip;
    public AudioSource aud_source;

    void Awake()
    {
        aud_source = gameObject.AddComponent<AudioSource>();
        aud_source.clip = audio_clip;

        if(ref_dont_destroy_audio == null)
            ref_dont_destroy_audio = this;
        else
        {
            Destroy(ref_dont_destroy_audio);
            Destroy(ref_dont_destroy_audio.aud_source);
            return;
        }
        DontDestroyOnLoad(ref_dont_destroy_audio);
    }


}
