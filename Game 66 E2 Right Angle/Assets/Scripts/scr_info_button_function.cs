﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scr_info_button_function : MonoBehaviour
{
    [SerializeField]scr_blue_screen ref_blue_screen;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]GameObject dog_screen_object;
    [SerializeField]Image How_to_Play_prompt;
    [SerializeField]Image Clue_prompt; 
    [SerializeField]Image Teacher_prompt;
    [SerializeField]GameObject How_to_Play_button_object;
    [SerializeField]GameObject Clue_button_object;
    [SerializeField]GameObject Teacher_button_object;
    [SerializeField]GameObject Skate_Icon_object;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    public GameObject x_mark_toggle_music;
    bool only_one_prompt = false;
    [SerializeField]bool is_Played = false;
    [SerializeField]bool is_complete_level = false;
    public static bool is_firstime_blink = true;
    public static scr_info_button_function instance;
    void Awake()
    {
        Singleton_Instance();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
        if(is_firstime_blink == true)
        {
            How_to_Play_button_Blink(-1);
        }
    }
    void Update()
    {
        Set_Info_Button();
        Set_Prompt_Off();
        Set_Skate_Icon_Button();    
    }
    void Singleton_Instance()
    {
        if(instance == null)
        {   
            instance = this;
            Debug.Log("Static: " + instance);
        }
        
        else if (instance != this) 
        {
            //Instance is not the same as the one we have, destroy old one, and reset to newest one
            Destroy(instance.gameObject);
            instance = this;
        }
        /*
        else
        {
            Destroy(gameObject);
            return;
        }
        */
        DontDestroyOnLoad(gameObject);
        Debug.Log("GameObject: " + gameObject);
    }
    public void Set_Prompt_Off()
    {
        if(How_to_Play_prompt.IsActive() || Clue_prompt.IsActive() || Teacher_prompt.IsActive())
        {   Debug.Log("Has Info");
            if(Input.GetMouseButtonDown(0))
            {
                Debug.Log("Has Pressed");
                How_to_Play_prompt.enabled = false;
                Clue_prompt.enabled = false;
                Teacher_prompt.enabled = false;
                only_one_prompt = false;
                ref_audio_manager_2.Play_Audio("info_retreat_sfx");
                How_to_Play_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                Clue_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                Teacher_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                How_to_Play_button_object.GetComponent<Button>().enabled = true;
                Clue_button_object.GetComponent<Button>().enabled = true;
                Teacher_button_object.GetComponent<Button>().enabled = true;
                // Fade to normal
                LeanTween.color(dog_screen_object.GetComponent<RectTransform>(),Color.white,0.5f);
                ref_card_manager.Set_Array_Card_Buttons_Bool(true);
            }
        }
    }

    public void How_To_Play()
    {
        Debug.Log("How to Play Button is Pressed!");
        if(How_to_Play_prompt.enabled == true && only_one_prompt == true)
        { 
            How_to_Play_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
            How_to_Play_prompt.enabled = false;
            only_one_prompt = false;
            ref_audio_manager_2.Play_Audio("info_retreat_sfx");

        }
        else if(only_one_prompt == false)
        {
            How_to_Play_prompt.enabled = true;
            only_one_prompt = true;
            How_to_Play_prompt.GetComponent<scr_tween_info_button>().Enlarge_Info_Tween();
            //ref_audio_manager.Play_Audio("skate_flip");
            ref_audio_manager_2.Play_Audio("info_but_press_sfx");
            How_to_Play_button_object.GetComponent<Button>().enabled = false;
            // Fade to Black
            LeanTween.color(dog_screen_object.GetComponent<RectTransform>(),Color.gray,0.5f);
            ref_card_manager.Set_Array_Card_Buttons_Bool(false);
        }
    }

    public void Clue()
    {
        Debug.Log("Clue Button is Pressed!");
        if(Clue_prompt.enabled == true && only_one_prompt == true)
        { 
            Clue_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
            Clue_prompt.enabled = false;
            only_one_prompt = false;
            ref_audio_manager_2.Play_Audio("info_retreat_sfx");

        }
        else if(only_one_prompt == false)
        {
            Clue_prompt.enabled = true;
            only_one_prompt = true;
            Clue_prompt.GetComponent<scr_tween_info_button>().Enlarge_Info_Tween();
            //ref_audio_manager.Play_Audio("skate_flip");
            ref_audio_manager_2.Play_Audio("info_but_press_sfx");
            Clue_button_object.GetComponent<Button>().enabled = false;
            // Fade to Black
            LeanTween.color(dog_screen_object.GetComponent<RectTransform>(),Color.gray,0.5f);
            ref_card_manager.Set_Array_Card_Buttons_Bool(false);
        }
    }

    public void Teacher()
    {
        Debug.Log("Teacher is Pressed!");
        if(Teacher_prompt.enabled == true && only_one_prompt == true)
        { 
            Teacher_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
            Teacher_prompt.enabled = false;
            only_one_prompt = false;
            ref_audio_manager_2.Play_Audio("info_retreat_sfx");

        }
        else if(only_one_prompt == false)
        {
            Teacher_prompt.enabled = true;
            only_one_prompt = true;
            Teacher_prompt.GetComponent<scr_tween_info_button>().Enlarge_Info_Tween();
            //ref_audio_manager.Play_Audio("skate_flip");
            ref_audio_manager_2.Play_Audio("info_but_press_sfx");
            Teacher_button_object.GetComponent<Button>().enabled = false;
            // Fade to Black
            LeanTween.color(dog_screen_object.GetComponent<RectTransform>(),Color.gray,0.5f);
            ref_card_manager.Set_Array_Card_Buttons_Bool(false);
        }
    }
    public void How_To_Play_Side_Feature()
    {
        LeanTween.cancel(How_to_Play_button_object.GetComponent<RectTransform>());
        LeanTween.color(How_to_Play_button_object.GetComponent<RectTransform>(),Color.white,0f);
    }
    public void Sound()
    {
        //Debug.Log("Sound is Pressed!");
        if(ref_audio_manager_2.Prop_Toggle_Music == true)
        ref_audio_manager_2.Prop_Toggle_Music = false;
        else//(if false)
        ref_audio_manager_2.Prop_Toggle_Music = true;

        if(ref_audio_manager_2.Prop_Toggle_Music == true)
        {

            if(is_complete_level == false )
            {
                //ref_audio_manager.Play_Audio("bg_audio");
                ref_audio_manager_2.Play_Audio("bg_audio_sfx");
            }
            else if(is_complete_level == true)
            {
                //ref_audio_manager.Play_Audio("bg_play_every_pg");
                ref_audio_manager_2.Play_Audio("bg_audio_sfx");
            }

            // Toggle Audio Off
            //ref_audio_manager_2.Prop_Toggle_Music = false;
            x_mark_toggle_music.SetActive(false);
        }   
        else// Off
        {
            if(is_complete_level == false)
            {
                //ref_audio_manager.Pause_Audio("bg_audio");
                ref_audio_manager_2.Pause_Audio("bg_audio_sfx");
            }
            else if(is_complete_level == true)
            {
                //ref_audio_manager.Pause_Audio("bg_play_every_pg");
                ref_audio_manager_2.Pause_Audio("bg_audio_sfx");
            }
            // Toggle Audio On
            //ref_audio_manager_2.Prop_Toggle_Music = true;
            x_mark_toggle_music.SetActive(true);
        }
    }

    public void Home()
    {
        Debug.Log("Home is Pressed!");
        ref_audio_manager_2.Play_Audio("home_sfx");
        if(is_Played == true)
        {
            is_firstime_blink = false;
            SceneManager.LoadScene(0);
        }
        else
        {   
            ref_audio_manager_2.Stop_Audio("bg_audio_sfx");
            SceneManager.LoadScene(1);
        }

    }

    public void Skate_Sound()
    {
        if(ref_audio_manager_2.Prop_Skate_Icon_Index >= ref_audio_manager_2.Prop_Get_Skate_Icon_Audio.Length)
        ref_audio_manager_2.Prop_Skate_Icon_Index = 0;

        if(ref_audio_manager_2.Prop_Skate_Icon_Index >= ref_audio_manager_2.Prop_Get_Skate_Icon_Audio.Length - 
        (ref_audio_manager_2.Prop_Get_Skate_Icon_Audio.Length - 1))
        {
            ref_audio_manager_2.Stop_Audio(ref_audio_manager_2.Prop_Get_Skate_Icon_Audio[ref_audio_manager_2.Prop_Skate_Icon_Index - 1].name,
            ref_audio_manager_2.Prop_Get_Skate_Icon_Audio);
        }

        ref_audio_manager_2.Play_Audio(ref_audio_manager_2.Prop_Get_Skate_Icon_Audio[ref_audio_manager_2.Prop_Skate_Icon_Index].name,
        ref_audio_manager_2.Prop_Get_Skate_Icon_Audio);
        ref_audio_manager_2.Prop_Skate_Icon_Index++;
        
    }

    // Determine if the Buttons are displayed on screen determine on the type of screen displayed(Dog and Blue)
    void Set_Info_Button()
    {   
        if(ref_blue_screen.Prop_Blue_Screen_Object.activeSelf == false && is_complete_level == false)
        {
            How_to_Play_button_object.SetActive(true);
            Clue_button_object.SetActive(true);
            Teacher_button_object.SetActive(true);
            Skate_Icon_object.SetActive(true);
            
        }
        else
        {
            How_to_Play_button_object.SetActive(false);
            Clue_button_object.SetActive(false);
            Teacher_button_object.SetActive(false);
            Skate_Icon_object.SetActive(false);
        }
    }

    void Set_Skate_Icon_Button()
    {
        if(is_complete_level == true)
        {
            Skate_Icon_object.SetActive(true);
        }
    }

    public GameObject Prop_How_to_Play_button_object
    {
        get{return How_to_Play_button_object;}
        set{How_to_Play_button_object = value;}
    }

    public GameObject Prop_Clue_button_object
    {
        get{return Clue_button_object;}
        set{Clue_button_object = value;}
    }
    public GameObject Prop_Teacher_button_object
    {
        get{return Teacher_button_object;}
        set{Teacher_button_object = value;}
    }

    public bool Prop_Is_Complete_Level
    {
        get{return is_complete_level;}
        set{is_complete_level = value;}
    }
    public bool Prop_Is_Played
    {
        get{return is_Played;}
        set{is_Played = value;}
    }

    public void How_to_Play_button_Blink(int count)
    {   
        LeanTween.color(How_to_Play_button_object.GetComponent<RectTransform>(),Color.grey,0.5f).setLoopPingPong(count);
    }

}
