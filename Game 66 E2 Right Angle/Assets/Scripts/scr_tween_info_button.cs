﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_info_button : MonoBehaviour
{
    
    [SerializeField]Vector3 max_default_size;
    [SerializeField]Vector3 smallest_size;
     const float time = 0.35f;

    public void Enlarge_Info_Tween()
    {
       
        LeanTween.scale(this.gameObject, max_default_size,time);
        
    }

    public void Reduce_Info_Tween()
    {
        LeanTween.scale(this.gameObject, smallest_size,time);
    }
}
