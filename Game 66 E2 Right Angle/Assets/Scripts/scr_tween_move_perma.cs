﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_move_perma : MonoBehaviour
{
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]GameObject ref_gameobject;
    [SerializeField]float vector_goal;
    Vector2 default_position;
    [SerializeField]float duration;
    [SerializeField]float delay;
    bool first_time_audio = false;
    void OnEnable()
    {
        Move_Tween();
    }
    public void Awake()
    {
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
        default_position = new Vector2(0.0f,-343.0f);
    }
    public void Move_Tween()
    {   
        LeanTween.moveLocalY(ref_gameobject,vector_goal,duration).setDelay(delay).setOnComplete(OnComplete).setEaseInOutBounce();
    }

    void OnComplete()
    {
        ref_audio_manager_2.Play_Audio("quote_drag_answer_sfx");
    }

    void OnDisable()
    {
        this.gameObject.GetComponent<RectTransform>().anchoredPosition = default_position;
    }

}
