﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class scr_audio_manager_2 : MonoBehaviour
{
    [SerializeField]bool toggle_music = true;
    [SerializeField]scr_sound[] sound;
    [SerializeField]scr_sound[] correct_vo;
    [SerializeField]int correct_vo_index;
    [SerializeField]scr_sound[] incorrect_vo;
    [SerializeField]int incorrect_vo_index;
    [SerializeField]scr_sound[] skate_icon_audio;
    [SerializeField]int skate_icon_index;
    public static scr_audio_manager_2 instance;
    void Awake()
    {
        foreach(scr_sound s in sound)
        {
            s.aud_source = gameObject.AddComponent<AudioSource>();
            s.aud_source.name = s.name;
            s.aud_source.clip = s.clip;
            s.aud_source.volume = s.volume;
            s.aud_source.loop = s.loop;
            s.aud_source.playOnAwake = s.awake;
        }
        foreach(scr_sound sc_co in correct_vo)
        {
            sc_co.aud_source = gameObject.AddComponent<AudioSource>();
            sc_co.aud_source.name = sc_co.name;
            sc_co.aud_source.clip = sc_co.clip;
            sc_co.aud_source.volume = sc_co.volume;
            sc_co.aud_source.loop = sc_co.loop;
            sc_co.aud_source.playOnAwake = sc_co.awake;
        }
        foreach(scr_sound sc_inc in incorrect_vo)
        {
            sc_inc.aud_source = gameObject.AddComponent<AudioSource>();
            sc_inc.aud_source.name = sc_inc.name;
            sc_inc.aud_source.clip = sc_inc.clip;
            sc_inc.aud_source.volume = sc_inc.volume;
            sc_inc.aud_source.loop = sc_inc.loop;
            sc_inc.aud_source.playOnAwake = sc_inc.awake;
        }
        foreach(scr_sound sc_sk_ic in skate_icon_audio)
        {
            sc_sk_ic.aud_source = gameObject.AddComponent<AudioSource>();
            sc_sk_ic.aud_source.name = sc_sk_ic.name;
            sc_sk_ic.aud_source.clip = sc_sk_ic.clip;
            sc_sk_ic.aud_source.volume = sc_sk_ic.volume;
            sc_sk_ic.aud_source.loop = sc_sk_ic.loop;
            sc_sk_ic.aud_source.playOnAwake = sc_sk_ic.awake;
        }
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()    
    {
        Play_Audio("bg_audio_sfx");
    }

    public void Play_Audio(string name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Play();
    }
    public void Play_Audio(string name, scr_sound[] array_vo)
    {
        scr_sound s_thing = Array.Find(array_vo,array_vos => array_vos.name == name);
        s_thing.aud_source.Play();
    }

    public void Pause_Audio(string name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Pause();
    }
    public void Stop_Audio(String name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Stop();
        
    }
    public void Stop_Audio(string name, scr_sound[] array_vo)
    {
        scr_sound s_thing = Array.Find(array_vo,array_vos => array_vos.name == name);
        s_thing.aud_source.Stop();
    }

    public bool Prop_Toggle_Music
    {
        get{return toggle_music;}
        set{toggle_music = value;}
    }

    public scr_sound[] Prop_Get_Correct_Vo
    {
        get{return correct_vo;}
    }
    public scr_sound[] Prop_Get_Incorrect_Vo
    {
        get{return incorrect_vo;}
    }
    public scr_sound[] Prop_Get_Skate_Icon_Audio
    {
        get{return skate_icon_audio;}
    }
    public int Prop_Skate_Icon_Index
    {
        get{return skate_icon_index;}
        set{skate_icon_index = value;}
    }
    public int Prop_Correct_Vo_Index
    {
        get{return correct_vo_index;}
        set{correct_vo_index = value;}
    }

    public int Prop_Incorrect_Vo_Index
    {
        get{return incorrect_vo_index;}
        set{incorrect_vo_index = value;}
    }

}
