﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class scr_card_button_function : MonoBehaviour
{
    [SerializeField]scr_blue_screen ref_Blue_screen;//Needed to render the questions
    [SerializeField]scr_card_manager ref_Card_manager;// The Card that gives out the question
    //[SerializeField]scr_audio_manager ref_audio_manager;// Needed to make those card sounds
    [SerializeField]scr_animation_manager ref_anim_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]scr_info_button_function ref_info_button_function;
    enum num_angle_choices{ONES,TWOS,THREES,FOURS}// an enum used for assigning which Card answers are coresponding...
    [SerializeField]scr_drop ref_scr_drop;//Needed to assign values of scr_drop with correct answer and card index for array

    void Awake()
    {
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    #region // Card Buttons

    public void Card_a()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_A());
    }
    public IEnumerator Card_A()
    {   
        Debug.Log("A is Pressed!");
    
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        
        // Makes Blue screen background render along with the question and answer cards
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        // Play the question card TWEEN Animation
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        // Sets the Question Card sprites to one of the question cards in the array
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[0].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {   // Display the Answer Cards dynamically
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[0].angle_choice_cards[i];//**********************************
            // Make the eventData.pointerDrag play the move UP TWEEN animation
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
            
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.TWOS; // set the correct answer depending on the Card Button
        ref_scr_drop.Prop_Card_Index = 0;// Tells the scr_drop which gameobject of cards will be used.
    }
    public void Card_b()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_B());
    }
    public IEnumerator Card_B()
    {
        Debug.Log("B is Pressed!");
        
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);

        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[1].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite =
            ref_Card_manager.Prop_Blue_Questionares[1].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.THREES;
        ref_scr_drop.Prop_Card_Index = 1;
    }
    public void Card_c()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_C());
    }
    public IEnumerator Card_C()
    {
        Debug.Log("C is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[2].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[2].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.FOURS;
        ref_scr_drop.Prop_Card_Index = 2;
    }

    public void Card_d()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_D());
    }

    public IEnumerator Card_D()
    {
        Debug.Log("D is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[3].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[3].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.TWOS;
        ref_scr_drop.Prop_Card_Index = 3; 
    }

    public void Card_e()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_E());
    }

    public IEnumerator Card_E()
    {
        Debug.Log("E is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[4].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[4].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud"); 
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.THREES;
        ref_scr_drop.Prop_Card_Index = 4;
    }

    public void Card_f()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_F());
    }

    public IEnumerator Card_F()
    {
        Debug.Log("F is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[5].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[5].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.FOURS;
        ref_scr_drop.Prop_Card_Index = 5;
    }

    public void Card_g()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_G());
    }

    public IEnumerator Card_G()
    {
        Debug.Log("G is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[6].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[6].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.ONES;
        ref_scr_drop.Prop_Card_Index = 6;
    }

    public void Card_h()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_H());
    }

    public IEnumerator Card_H()
    {
        Debug.Log("H is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[7].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[7].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.FOURS;
        ref_scr_drop.Prop_Card_Index = 7;
    }

    public void Card_i()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_I());
    }

    public IEnumerator Card_I()
    {
        Debug.Log("I is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[8].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[8].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.TWOS;
        ref_scr_drop.Prop_Card_Index = 8;
    }
    public void Card_j()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_J());
    }

    public IEnumerator Card_J()
    {
        Debug.Log("J is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[9].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[9].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.THREES;
        ref_scr_drop.Prop_Card_Index = 9;
    }

    public void Card_k()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_K());
    }

    public IEnumerator Card_K()
    {
        Debug.Log("K is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[10].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[10].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud"); 
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.TWOS;
        ref_scr_drop.Prop_Card_Index = 10;
    }
    public void Card_l()
    {   
        ref_audio_manager_2.Play_Audio("card_click_sfx");
        ref_info_button_function.Prop_Is_Played = true;
        StartCoroutine(Card_L());
    }
    public IEnumerator Card_L()
    {
        Debug.Log("L is Pressed!");
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        yield return new WaitForSeconds(0.50f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(true);
        ref_Card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
        ref_Card_manager.Prop_Question_Card.sprite = ref_Card_manager.Prop_Blue_Questionares[11].question_card;
        for(int i = 0; i < ref_Card_manager.Prop_Angle_Choice_Cards.Length;i++)
        {  
            ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<Image>().sprite = 
            ref_Card_manager.Prop_Blue_Questionares[11].angle_choice_cards[i];
            //ref_Card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
        }
        //ref_audio_manager.Play_Audio("to_blue_screen_aud");
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx"); 
        ref_scr_drop.Prop_The_Correct_Answer = (int)num_angle_choices.FOURS;
        ref_scr_drop.Prop_Card_Index = 11;
    }
}
#endregion