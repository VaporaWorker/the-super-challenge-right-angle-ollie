﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_animation_manager : MonoBehaviour
{
    [SerializeField]Animator anmtr_H_transition; 
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    //[SerializeField]Animator anmtr_V_transition;

    void Update()
    {
        
        
    }



    public void Play_H_Blue_Transition()
    {
        anmtr_H_transition.Play("to_blue_H");
    }

    public void Play_H_Dog_Transition()
    {
        anmtr_H_transition.Play("to_dog_H");

    }

    public Animator Prop_H_Transition
    {
        get{return anmtr_H_transition;}
        set{anmtr_H_transition = value;}
    }

    /*public Animator Prop_V_Transition
    {
        get{return anmtr_V_transition;}
        set{anmtr_V_transition = value;}
    }
    */
}
