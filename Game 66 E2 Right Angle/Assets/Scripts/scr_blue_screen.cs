﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_blue_screen : MonoBehaviour
{   
    [SerializeField]GameObject blue_screen_object;//Needed to render the questions
    [SerializeField] bool bool_time_blue_screen = false;


    public GameObject Prop_Blue_Screen_Object
    {
        get{return blue_screen_object;}
        set{blue_screen_object = value;}
    }

    public bool Prop_Bool_Time_Blue_Screen
    {
        get{return bool_time_blue_screen;}
        set{bool_time_blue_screen = value;}
    }
}
