﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class scr_drop : MonoBehaviour, IDropHandler
{

   [SerializeField]scr_blue_screen ref_Blue_screen; // blue screen gameobject
   [SerializeField]scr_set_skate_default_location ref_set_skte_def_loc;
   [SerializeField]scr_card_lose_restart_function ref_card_lose_restart_function;
   [SerializeField]scr_card_manager ref_card_manager;
   [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
   [SerializeField]scr_card_win_restart_function ref_win_restart;
   [SerializeField]scr_guess_condition ref_guess_condition;
   [SerializeField]scr_animation_manager ref_anim_manager;
   [SerializeField]scr_info_button_function ref_info_button;
   [SerializeField]int the_correct_answer; // a value meant for the answer/angle card array to cycle thru them
   [SerializeField]int card_index; // a value meant for the skateboard card arrays to cycle thru
   [SerializeField]bool isLocked_in_place = false;
   public bool Prop_IsLocked_Place
   {
       get{return isLocked_in_place;}
       set{isLocked_in_place = value;}
   }

    void Awake()
    {
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Drop Drag");
        // Sets the answer card locked to prevent further movement upon being placed on top of the question card
        isLocked_in_place = true;
        ref_card_manager.Prop_Angle_Choice_Cards[eventData.pointerDrag.GetComponent<scr_drag_drop>().Choice_Value].
        GetComponent<RectTransform>().anchoredPosition = ref_card_manager.Prop_Question_Card.GetComponent<RectTransform>().
        anchoredPosition;
        // Resets the Angle_Cards to its default canvas group state
        eventData.pointerDrag.GetComponent<CanvasGroup>().alpha = 1f;
        eventData.pointerDrag.GetComponent<CanvasGroup>().blocksRaycasts = true;
        for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = false;
        }
        ref_audio_manager_2.Play_Audio("drop_answer_sfx");
        // Set all Array Cards buttons to false
        Set_Array_Card_Buttons_False();
        // Prvents Users from pressing the info buttons
        ref_info_button.Prop_How_to_Play_button_object.GetComponent<Button>().enabled = false;
        ref_info_button.Prop_Clue_button_object.GetComponent<Button>().enabled = false;
        ref_info_button.Prop_Teacher_button_object.GetComponent<Button>().enabled = false;
        //WHEN THE PLAYER ANSWERED IT CORRECTLY
        // Checks if evenData.pointerDrag matches the ref_Angle_choice_card with the correct answer
        if(eventData.pointerDrag == ref_card_manager.Prop_Angle_Choice_Cards[the_correct_answer])
        {   
            StartCoroutine(Correct_Answer(eventData.pointerDrag));
            
        }
        else// WHEN THE PLAYER ANSWERED IT INCORRECTLY
        {
            
            if(ref_guess_condition.Prop_Guesses >= 0)   
            {   Debug.Log("Wrong Answer");
                // Set the the skate_card_index and answer_cor_card to its INCORRECT value
                ref_card_manager.Prop_Array_Cards[card_index].GetComponent<scr_wrong_skate>().answer_wrong_card = 
                eventData.pointerDrag.GetComponent<scr_drag_drop>().Choice_Value;
                StartCoroutine(Incorrect_Answer());
            }
        }
        //eventData.pointerDrag.GetComponent<scr_drag_drop>().Prop_Is_Draggable = true;
    }

    public IEnumerator Correct_Answer(GameObject eventData_holder)
    {   

        //Reset Guesses for each correct 
        ref_guess_condition.Prop_Guesses = 2;
        //Resets the index for the array of  
        ref_card_lose_restart_function.Prop_Record_Wrong_Answer_Index = 0;
        ref_card_lose_restart_function.Prop_Record_Check_Array_Index = 0;
        // Delay for Transition
        yield return new WaitForSeconds(1.0f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_dog_H",true);
        ref_audio_manager_2.Play_Audio("swoosh_blue_screen_sfx");
        yield return new WaitForSeconds(0.5f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_dog_H",false);
        isLocked_in_place = false;
        // Set Draggable to true
        for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = true;
        }
        Set_Def_Location_Answer_Cards();
        // Set the the skate_card_index and answer_cor_card to its CORRECT value
        ref_card_manager.Prop_Array_Cards[card_index].GetComponent<scr_tween_skate>().answer_card = the_correct_answer;
        // Warning Sign Off
        if(ref_win_restart.warning_sign.activeSelf == true)
        {
            ref_win_restart.warning_sign.SetActive(false); 
        }
        // Set scr_tween_skate to true
        ref_card_manager.Prop_Array_Cards[card_index].GetComponent<scr_tween_skate>().enabled  = true;
        // Makes the Skateboard button off 
        ref_card_manager.Prop_Array_Cards[card_index].GetComponent<Button>().enabled = false;
        
        // Set Blue Screen off
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(false);
        //ref_audio_manager.Play_Audio("after_card_dog_screen");
        yield return new WaitForSeconds(0.15f);
        
    }

    public IEnumerator Incorrect_Answer()
    {  

        // Delay for Transition
        yield return new WaitForSeconds(1.0f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_dog_H",true);
        ref_audio_manager_2.Play_Audio("swoosh_blue_screen_sfx");
        yield return new WaitForSeconds(0.5f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_dog_H",false);
        isLocked_in_place = false;
        for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = true;
        }
        Set_Def_Location_Answer_Cards();
         // Set scr_tween_skate to true APPLIES THE WRONG SKATE FEATURE
        ref_card_manager.Prop_Array_Cards[card_index].GetComponent<scr_wrong_skate>().enabled  = true;

        
        ref_Blue_screen.Prop_Blue_Screen_Object.SetActive(false);
        
    }
    public void Set_Def_Location_Answer_Cards()
    {
        // Set evenData.pointerDrag(Answer Card) to its default location after dropping on the Skateboard
        for(int i = 0; i < ref_set_skte_def_loc.GetComponent<scr_set_skate_default_location>().
            Prop_Default_Angle_Choice_Location.Length; i++)
        {
            
            ref_card_manager.GetComponent<scr_card_manager>().Prop_Angle_Choice_Cards[i].GetComponent<RectTransform>().
            anchoredPosition = ref_set_skte_def_loc.GetComponent<scr_set_skate_default_location>().Prop_Default_Angle_Choice_Location[i];            
        }
    }
    public void Set_Array_Card_Buttons_False()
    {
        for(int i = 0;i < ref_card_manager.Prop_Array_Cards.Length;i++)
        {   // Set all Array Cards buttons to false
            ref_card_manager.Prop_Array_Cards[i].GetComponent<Button>().enabled = false;
        }
    }

    public int Prop_The_Correct_Answer
    {
        get{return the_correct_answer;}
        set{the_correct_answer = value;}
    }
    public int Prop_Card_Index
    {
        get{return card_index;}
        set{card_index = value;}
    }
}
