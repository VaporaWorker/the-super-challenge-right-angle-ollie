﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_guess_condition : MonoBehaviour
{
    [SerializeField]int guesses = 2; // Lives for the player. When it reaches 0, the game restarts
    [SerializeField]int num_answ_correctly; // checks to see if anyone reach 12 then show win screen

    public int Prop_Guesses
    {
        get{return guesses;}
        set{guesses = value;}
    }
    
    public int Prop_Num_Answered_Correctly 
    {
        get{return num_answ_correctly;}
        set{num_answ_correctly = value;}
    }
}
