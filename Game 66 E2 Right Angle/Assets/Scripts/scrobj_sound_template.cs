﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "audio sound clips")]
public class scrobj_sound_template:ScriptableObject
{
    public string name;
    public AudioClip clip;
    [Range(0f,1f)]
    public float volume;
    public bool awake;
    public bool loop;
    [HideInInspector]
    public AudioSource aud_source;
}
