﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_animtr_explosion : MonoBehaviour
{
    [SerializeField]Animator anim;


    public void Animation_Begin()
    {
        anim.SetBool("can_explode",true);
    }

    public void Animation_End()
    {
        anim.SetBool("can_explode",false);
    }
}
