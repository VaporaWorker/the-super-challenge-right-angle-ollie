﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scr_card_lose_restart_function : MonoBehaviour
{
    
    //[SerializeField] scr_audio_manager ref_audio_manager;
    [SerializeField] scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField] scr_double_x_mark ref_double_x_mark;
    [SerializeField] scr_card_manager ref_card_manager;
    [SerializeField] GameObject restart_object;
    int record_wrong_answer_index;
    [SerializeField] int[] recorded_wrong_answer_value;
    int record_check_array_index;
    [SerializeField]bool[] record_check_array;

    void OnEnable()
    {
        StartCoroutine(Lose_Screen());
        
    }

    void Awake()
    {
        //ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }
    // START THE LOSE SCREEN
    IEnumerator Lose_Screen()   
    {
        
        
        for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = false;
        }
        yield return new WaitForSeconds(2.5f);
        for(int i = 0 ; i < ref_double_x_mark.Prop_X_Dog_Mark.Capacity; i++)
        {
            
            if(record_check_array[i] == true)
            {
                // Adds delay for the x mark to be enabled
                yield return new WaitForSeconds(1.0f);
                // Set X Mark Enabled to true to be displayed
                ref_double_x_mark.Prop_X_Dog_Mark[i].SetActive(true);
                // Set the postion of Double X Mark from the position of Angle/ Answer Cards 
                ref_double_x_mark.Prop_X_Dog_Mark[i].GetComponent<RectTransform>().anchoredPosition = 
                ref_card_manager.Prop_Angle_Choice_Cards[recorded_wrong_answer_value[i]].GetComponent<RectTransform>().anchoredPosition;

                // Set the audio to one of the 3 random audios for the wrong sfx
                int aud_num = Random.Range(0,3);
                string audio_answer = (aud_num == 0) ? "inc_a":(aud_num == 1) ? "inc_b":(aud_num == 2) ? "inc_c" : "no audio match";
                //ref_audio_manager.Play_Audio(audio_answer);
                ref_audio_manager_2.Play_Audio(audio_answer);
            }

        }

        yield return new WaitForSeconds(1.35f);
        //Play Audio
        //ref_audio_manager.Play_Audio("after_2nd_ans_wrong_again");
        //ref_audio_manager.Play_Audio("stamp_sfx");

        ref_audio_manager_2.Play_Audio("second_wrong_answer_sfx");
        yield return new WaitForSeconds(1.75f);
        ref_audio_manager_2.Play_Audio("restart_sfx");

        // Make the restart image appear
        restart_object.SetActive(true);
       
        yield return new WaitForSeconds(2.0f);
        // Restarts the game
       
        SceneManager.LoadScene(0);
        scr_info_button_function.is_firstime_blink = false;
        //ref_audio_manager.Play_Audio("after_2nd_ans_wrong_return_dog");
        ref_audio_manager_2.Play_Audio("after_start_again_vo_oh_no_sfx");
        //ref_dont_destroy_audio.aud_source.PlayOneShot(ref_dont_destroy_audio.audio_clip);
    }

    public int[] Prop_Recorded_Wrong_Answer_Value
    {
        get{return recorded_wrong_answer_value;}
        set{recorded_wrong_answer_value = value;}
    }

    public int Prop_Record_Wrong_Answer_Index
    {
        get{return record_wrong_answer_index;}
        set{
           if(record_wrong_answer_index < 3)
           {
               record_wrong_answer_index = value;
           } 
        }
    }

    public bool[] Prop_Record_Check_Array
    {
        get{return record_check_array;}
        set{record_check_array = value;}
    }
    public int Prop_Record_Check_Array_Index
    {
        get{return record_check_array_index;}
        set{record_check_array_index = value;}
    }
}
