﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_xmark : MonoBehaviour
{
    [SerializeField]float duration;
    [SerializeField]Vector3 scale_goal;
    [SerializeField]Vector3 default_scale;
    public void Xmark_Tween()
    {
        LeanTween.scale(this.gameObject,scale_goal,duration);
    }

    void OnEnable()
    {
        Xmark_Tween();
    }    

    void OnDisable()
    {
        this.gameObject.GetComponent<RectTransform>().localScale = default_scale;
    }

}
