﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_move : MonoBehaviour
{
   [SerializeField]GameObject ref_gameobject;
   [SerializeField]float vector_goal;
   [SerializeField]float duration;
   [SerializeField]float delay;

    void OnEnable()
    {
        Move_Tween();
    }

    public void  Move_Tween()
    {
        LeanTween.moveLocalY(ref_gameobject,vector_goal,duration).setOnComplete(Fade_Tween);
    }

    public void Fade_Tween()
    {
        LeanTween.alpha(ref_gameobject.GetComponent<RectTransform>(),0.0f,duration).setDelay(delay).setOnComplete(Disable_Object);
    }

    public void Disable_Object()
    {
        ref_gameobject.SetActive(false);
    }
}
