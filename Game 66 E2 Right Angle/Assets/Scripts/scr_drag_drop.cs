﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class scr_drag_drop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]scr_set_skate_default_location ref_skate_def_loc;
    [SerializeField]scr_drop ref_drop;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]RectTransform rectTransform;
    [SerializeField]CanvasGroup canvasGroup;
    [SerializeField]Canvas canvas;
    //Vector2 UI_Position;
    [SerializeField]int choice_value;
    [SerializeField]bool is_dragable = true;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
        
    }

    void Update()
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(is_dragable == true)
        {
            //Debug.Log("Begin Drag");
            canvasGroup.alpha = .6f;
            canvasGroup.blocksRaycasts = false;
            int aud_num = Random.Range(0,4);
            string audio_answer = (aud_num == 0) ? "1_card_drag_swoosh":(aud_num == 1) ? "2_card_drag_swoosh"
                                :(aud_num == 2) ? "3_card_drag_swoosh" :(aud_num == 3) ? "3_card_drag_swoosh" :  "no audio match";
            ref_audio_manager_2.Play_Audio(audio_answer);
        }

    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Draggng");
        if(is_dragable == true)
        {
            rectTransform.anchoredPosition += eventData.delta/canvas.scaleFactor;
            
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("End Drag");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        if(ref_drop.Prop_IsLocked_Place == false)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = 
            ref_skate_def_loc.Prop_Default_Blue_Angle_Choice_Location[eventData.pointerDrag.GetComponent<scr_drag_drop>().choice_value];
        }

    }

    public int Choice_Value
    {
        get{return choice_value;}
        set{choice_value = value;}
    }

    public bool Prop_Is_Draggable
    {
        get{return is_dragable;}
        set{is_dragable = value;}
    }

    public CanvasGroup Prop_CanvasGroup
    {
        get{return canvasGroup;}
        set{canvasGroup = value;}
    }
}
