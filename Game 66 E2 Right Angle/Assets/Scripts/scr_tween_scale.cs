﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_scale : MonoBehaviour
{
    [SerializeField]GameObject ref_gameObject;
    [SerializeField]Vector3 scale_goal;
    [SerializeField]float duration;
    [SerializeField]float delay;

    public void OnEnable()
    {
        Scale_Tween(); 
    }
    public void Scale_Tween()
    {
        LeanTween.scale(ref_gameObject,scale_goal,duration).setOnComplete(Fade_Tween);
    }
    public void Fade_Tween()
    {
        LeanTween.alpha(ref_gameObject.GetComponent<RectTransform>(),0.0f,duration).setDelay(delay).setOnComplete(Disable_Object);
    }

    public void Disable_Object()
    {
        ref_gameObject.SetActive(false);
    }
}
