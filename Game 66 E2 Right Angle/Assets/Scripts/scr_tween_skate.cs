﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

public class scr_tween_skate : MonoBehaviour
{
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]GameObject ref_win_lose_guess_restart; 
    [SerializeField]scr_guess_condition ref_guess_condition;
    [SerializeField]scr_animtr_explosion ref_animtr_explosion;
    [SerializeField]scr_tween_vertical_transition ref_tween_vert_tran;
    [SerializeField]GameObject skate_card;
    [SerializeField]float RotationZ;
    [SerializeField]float rot_duration;
    [SerializeField]float shake_duration;
    [SerializeField]float delay;
    [SerializeField]int skate_card_index;
    public int answer_card;

    void OnEnable()
    {
        if(ref_win_lose_guess_restart.GetComponent<scr_guess_condition>().Prop_Guesses < 2)
        {   
            // Set the Record Check Index value to true each time a player is CORRECT
            ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Check_Array[0] = false;
        }
        StartCoroutine(Delay_Card()); 
    }
    void Awake()
    {
        //ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    void Start()
    {
        skate_card = this.gameObject;
    }
    public void Shake_Tween()
    {
        LeanTween.rotateZ(this.skate_card,15,shake_duration).setLoopPingPong(4);
        ref_audio_manager_2.Play_Audio("card_shake_sfx");
    }
    public IEnumerator Delay_Card()
    {   
        yield return new WaitForSeconds(0.5f);
        //****
        Shake_Tween();
        // Swap Skateboard Cards with one of the 4 Answer Cards
        yield return new WaitForSeconds(delay-0.75f);
        // Set the Array Cards sprite into the correct angle/answer cards
        ref_card_manager.Prop_Array_Cards[skate_card_index].GetComponent<Image>().sprite = 
        ref_card_manager.Prop_Angle_Choice_Cards[answer_card].GetComponent<Image>().sprite;

        //Play Correct Audio
        ref_audio_manager_2.Play_Audio("correct_answer_on_skat_sfx");
        StartCoroutine(Delay_Rotate_Card());       
    }

    public IEnumerator Delay_Rotate_Card()
    {   
        Debug.Log("Delay_Rotate_Card");
        
        // Checks if the Correct Index is above the Correct Vo Length, Resets the index to 0
        if(ref_audio_manager_2.Prop_Correct_Vo_Index >= ref_audio_manager_2.Prop_Get_Correct_Vo.Length)
        ref_audio_manager_2.Prop_Correct_Vo_Index = 0;
        yield return new WaitForSeconds(1.15f);
        // Plays the Audio of Correct_Vo that CYCLES via Correct_Vo_Index
        ref_audio_manager_2.Play_Audio(ref_audio_manager_2.Prop_Get_Correct_Vo[ref_audio_manager_2.Prop_Correct_Vo_Index].name,
        ref_audio_manager_2.Prop_Get_Correct_Vo);
        // Cycle thru the Correct_Vo Array to switch Audios
        ref_audio_manager_2.Prop_Correct_Vo_Index++;
        //ref_audio_manager.Play_Audio(audio_VO_answer);
        //ref_audio_manager_2.Play_Audio(audio_VO_answer_2);
        yield return new WaitForSeconds(delay-1.0f);
        //ref_audio_manager.Play_Audio("crct_card_spin_aud");
        ref_audio_manager_2.Play_Audio("cor_spin_sfx");

        // Plays the Audio       
    
        Rotate_Tween_Z();
    }

    public void Rotate_Tween_Z()
    {
        // Play Rotate Tween
        LeanTween.rotateZ(skate_card,RotationZ,rot_duration).setOnComplete(Disable_Correct);
    }

    void Disable_Correct()
    {
        
        // Set off the gameobject
        skate_card.SetActive(false);
        // Increment the num of answered correctly to fulfil WIN SCREEN CONDITION 
        ref_guess_condition.Prop_Num_Answered_Correctly++;
        if(ref_guess_condition.Prop_Num_Answered_Correctly >= 12)
        ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().enabled = true;
        else
        ref_tween_vert_tran.GetComponent<scr_tween_vertical_transition>().gameObject.SetActive(true);
        // Resets the delays
        delay = 2.0f;
        /*
        for(int i = 0; i < ref_card_manager.Prop_Array_Cards.Length;i++)
        {
            ref_card_manager.Prop_Array_Cards[i].GetComponent<Button>().enabled = true;
        }
        */
        //ref_audio_manager.Play_Audio("explosion_sfx");
        ref_audio_manager_2.Play_Audio("explo_sfx");
        
    }
    void OnDisable()
    {
        this.gameObject.GetComponent<scr_tween_skate>().enabled = false;
        ref_animtr_explosion.GetComponent<RectTransform>().anchoredPosition = 
        skate_card.GetComponent<RectTransform>().anchoredPosition;
        ref_animtr_explosion.Animation_Begin();
        
        
    }

    public int Prop_Skate_Card_Index
    {
        get{return skate_card_index;}
        set{skate_card_index = value;}
    }

}
