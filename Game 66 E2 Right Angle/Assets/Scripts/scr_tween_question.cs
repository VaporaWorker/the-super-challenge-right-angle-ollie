﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_question : MonoBehaviour
{
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]GameObject question_card;
    [SerializeField]float VectorY;
    [SerializeField]Vector2 default_position;
    [SerializeField]float duration;
    void Awake()
    {
        default_position = this.gameObject.GetComponent<RectTransform>().anchoredPosition;
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    void OnEnable()
    {
        StartCoroutine(Move_Tween_Question());
        
    }

    public IEnumerator Move_Tween_Question()
    {
        yield return new WaitForSeconds(3.0f);
        LeanTween.moveLocalY(question_card,VectorY,duration).setEaseOutBack();
        Play_Audio();
    }
    void Play_Audio()
    {
        //ref_audio_manager.Play_Audio("question_flip");
        ref_audio_manager_2.Play_Audio("question_card_flip_sfx");
    }

    void OnDisable()
    {
        this.gameObject.GetComponent<RectTransform>().anchoredPosition = default_position;
    }
}
