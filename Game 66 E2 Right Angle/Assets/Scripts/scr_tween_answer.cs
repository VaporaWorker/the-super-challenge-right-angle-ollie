﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_answer : MonoBehaviour
{   
    [SerializeField]scr_set_skate_default_location ref_skate_def_loc;
    [SerializeField]GameObject answer_card;
    [SerializeField]float VectorY;
    [SerializeField]float duration;
    [SerializeField]float delay;
    [SerializeField]float delay_sound;
    public int x_default_value_index;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    void Awake()
    {
        //ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();    
    }

    void OnEnable()
    {  
        StartCoroutine(Move_Tween_Answer());
        StartCoroutine(Play_Audio());
    }

    public IEnumerator Move_Tween_Answer()
    {    
        yield return new WaitForSeconds(delay);
        LeanTween.moveLocalY(answer_card,VectorY,duration).setOnComplete(OnComplete).setEaseInOutBack();
    }

    public IEnumerator Play_Audio()
    {
        yield return new WaitForSeconds(delay_sound);
        ref_audio_manager_2.Play_Audio("card_flip_sfx");   
    }
    
    public void OnComplete()
    {  
        Debug.Log("Duration elapsed");
        //ref_audio_manager.Play_Audio("four_ans_cards_dealt_aud");
        // Set the x positions of answer cards to the array 
        //ref_skate_def_loc.x_default_value[x_default_value_index] = this.gameObject.GetComponent<RectTransform>().localPosition.x;
        ref_skate_def_loc.Prop_Default_Blue_Angle_Choice_Location[x_default_value_index] = 
        this.gameObject.GetComponent<RectTransform>().anchoredPosition;
    }
}
