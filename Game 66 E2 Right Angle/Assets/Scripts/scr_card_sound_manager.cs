﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_card_sound_manager : MonoBehaviour
{   
    [SerializeField]AudioSource card_button_manager_audio_source;
    [SerializeField] AudioClip[] card_sounds;
    [SerializeField] AudioClip[] card_incorrect_sounds;
    [SerializeField] AudioClip[] card_lose_sounds;
    [SerializeField] AudioClip[] card_correct_sounds;
    [SerializeField] AudioClip[] warning_sound;
    [SerializeField]float warning_delay = 1.5f;

    public void Switch_To_Blue_Screen_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(card_sounds[0]);
    }
    public void Card_Set_Up_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(card_sounds[1]);
    }

    public void Answer_Card_Drag_Audio()
    {
        
        card_button_manager_audio_source.PlayOneShot(card_sounds[2]);
        //card_button_manager_audio_source.volume = 0.0f;
    }

    public void Correct_Spin_Answer_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(card_sounds[3]);
    }

    public void Correct_Post_Spin_Answer_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(card_sounds[4]);
    }

    public void Random_Incorrect_Answer_Audio()
    {
        int chosen_incorrect_audio = Random.Range(0,2);
        card_button_manager_audio_source.PlayOneShot(card_incorrect_sounds[chosen_incorrect_audio]);
    }

    public void Random_Correct_Answer_Audio()
    {
        int chosen_incorrect_audio = Random.Range(0,3);
        card_button_manager_audio_source.PlayOneShot(card_correct_sounds[chosen_incorrect_audio]);
    }
    public IEnumerator Warning_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(warning_sound[0]);
        yield return new WaitForSeconds(warning_delay);
        Danger_Warning_Audio();
    }

    public void Danger_Warning_Audio()
    {
        card_button_manager_audio_source.PlayOneShot(warning_sound[1]);
    }
}
