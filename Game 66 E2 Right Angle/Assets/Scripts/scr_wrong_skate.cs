﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_wrong_skate : MonoBehaviour
{
    [SerializeField]scr_blue_screen ref_blue_screen;
    [SerializeField]scr_tween_skate ref_tween_skate;
    [SerializeField]scr_card_manager ref_card_manager;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager_2 ref_audio_manager_2;
    [SerializeField]scr_double_x_mark ref_double_x_mark;
    [SerializeField]scr_animation_manager ref_anim_manager;
    [SerializeField]GameObject ref_win_lose_guess_restart;
    public float delay_wrong = 1.5f;
    public int answer_wrong_card;
    //public bool is_apply_delayed_anim_incorrect;
    
    void Awake()
    {
        
        ref_tween_skate = this.GetComponent<scr_tween_skate>();
        //ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_audio_manager_2 = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    void OnEnable()
    {   // Set the Wrong Card Value
        ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Recorded_Wrong_Answer_Value
        [ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Wrong_Answer_Index] = answer_wrong_card;
        ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Wrong_Answer_Index++;

        // Set the Record Check Index value to false each time a player is INCORRECT
        ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Check_Array
        [ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Check_Array_Index] = true;
        ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().Prop_Record_Check_Array_Index++;

        StartCoroutine(Delay_Wrong_Card());
    }

    // This happens in the DOG SCREEN
    public IEnumerator Delay_Wrong_Card()
    { 
        yield return new WaitForSeconds(0.5f);
        //Play Tween Shake Animation
        this.gameObject.GetComponent<scr_tween_skate>().Shake_Tween();
        // Swap Skateboard Cards with one of the 4 Answer Cards
        yield return new WaitForSeconds(delay_wrong);
        Debug.Log("Delay_Correct_Card");
        // Set the Array Cards sprite into the correct angle/answer cards
        ref_card_manager.Prop_Array_Cards[ref_tween_skate.Prop_Skate_Card_Index].GetComponent<Image>().sprite = 
        ref_card_manager.Prop_Angle_Choice_Cards[answer_wrong_card].GetComponent<Image>().sprite;

        // Randomly set a value to determine the audio
        int aud_num = Random.Range(0,3);
        //int aud_vo_num = Random.Range(0,2);
        string audio_answer_2 = (aud_num == 0) ? "inc_a":(aud_num == 1) ? "inc_b":(aud_num == 2) ? "inc_c" : "no audio match";

        // Plays the Audio
        //ref_audio_manager.Play_Audio(audio_answer);
        ref_audio_manager_2.Play_Audio(audio_answer_2);
        ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().x_mark_dog_screen.SetActive(true);
        // Set the x_Mark in different locations
        ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().x_mark_dog_screen.GetComponent<RectTransform>().
        anchoredPosition = ref_card_manager.Prop_Array_Cards[ref_tween_skate.Prop_Skate_Card_Index].GetComponent<RectTransform>().
        anchoredPosition; 
        // Deduct 1 Guess in the number of Guesses
        ref_win_lose_guess_restart.GetComponent<scr_guess_condition>().Prop_Guesses--;
        yield return new WaitForSeconds(delay_wrong - 0.75f);
        //ref_audio_manager.Play_Audio(audio_VO_answer);
        //ref_audio_manager_2.Play_Audio(audio_VO_answer_2);
        
        // Checks if the Incorrect Index is above the Incorrect Vo Length, Resets the index to 0
        if(ref_audio_manager_2.Prop_Incorrect_Vo_Index >= ref_audio_manager_2.Prop_Get_Incorrect_Vo.Length)
        ref_audio_manager_2.Prop_Incorrect_Vo_Index = 0;
        // Plays the Incorrect Audio of Correct_Vo that CYCLES via Correct_Vo_Index
        ref_audio_manager_2.Play_Audio(ref_audio_manager_2.Prop_Get_Incorrect_Vo[ref_audio_manager_2.Prop_Incorrect_Vo_Index].name,
        ref_audio_manager_2.Prop_Get_Incorrect_Vo);
        // Cycle thru the Correct_Vo Array to switch Audios
        ref_audio_manager_2.Prop_Incorrect_Vo_Index++;

        yield return new WaitForSeconds(delay_wrong + 0.5f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
        // play to blue screen audio
        ref_audio_manager_2.Play_Audio("zwoop_blue_screen_sfx");
        // Delay for Transitions
        yield return new WaitForSeconds(0.5f);
        ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
        // GOING TO THE BLUE SCREEN
        // Check if blue screen object is inactive, if true, enable blue screen
        if(ref_blue_screen.Prop_Blue_Screen_Object.activeSelf == false)
        {   
            // Set the x mark dog screen to false
            ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().x_mark_dog_screen.SetActive(false);
            ref_blue_screen.Prop_Blue_Screen_Object.SetActive(true);

            
            // Set the scr_wrong script on a certain skate card index
            ref_card_manager.Prop_Array_Cards[ref_tween_skate.Prop_Skate_Card_Index].GetComponent<scr_wrong_skate>().enabled = false;
            //ref_card_manager.Prop_Angle_Choice_Cards
            
            if(ref_win_lose_guess_restart.GetComponent<scr_guess_condition>().Prop_Guesses > 0)
            {

                // Make the eventData.pointerDrag play the move UP TWEEN animation
                for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
                {
                    ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_tween_answer>().Move_Tween_Answer();
                    ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = false;
                }
                // Set the Array Cards back to each default image
                for(int i = 0; i < ref_card_manager.Prop_Array_Cards.Length; i++)
                {
                    ref_card_manager.Prop_Array_Cards[i].GetComponent<Image>().sprite =
                    ref_card_manager.Prop_Array_Image_Cards[i];
                    
                }
                

                yield return new WaitForSeconds(4.5f);
                for(int i = 0; i < ref_card_manager.Prop_Angle_Choice_Cards.Length; i++)
                {
                    ref_card_manager.Prop_Angle_Choice_Cards[i].GetComponent<scr_drag_drop>().Prop_Is_Draggable = true;
                }
                ref_audio_manager_2.Play_Audio(audio_answer_2);
                // Set first double_x_mark to active
                ref_double_x_mark.Prop_X_Dog_Mark[0].SetActive(true);
                // Set the postion of Double X Mark to the position of Angle/ Answer Cards 
                ref_double_x_mark.Prop_X_Dog_Mark[0].GetComponent<RectTransform>().anchoredPosition = 
                ref_card_manager.Prop_Angle_Choice_Cards[answer_wrong_card].GetComponent<RectTransform>().anchoredPosition;

                yield return new WaitForSeconds(0.5f);
                // Set Warning Sign enabled
                ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().warning_sign.SetActive(true);
                // Tween Enlarge warning
                ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().warning_sign.GetComponent<scr_tween_warning>().
                Enlarge_Warning_Tween();
                //ref_card_manager.Prop_Question_Card.GetComponent<scr_tween_question>().Move_Tween_Question();
            }
            else if(ref_win_lose_guess_restart.GetComponent<scr_guess_condition>().Prop_Guesses <= 0)
            {   
                ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
                // Delay for Transitions
                yield return new WaitForSeconds(0.5f);
                ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
                
                ref_win_lose_guess_restart.GetComponent<scr_card_win_restart_function>().warning_sign.SetActive(false);
                ref_win_lose_guess_restart.GetComponent<scr_card_lose_restart_function>().enabled = true;
            }
        }
    }
}
