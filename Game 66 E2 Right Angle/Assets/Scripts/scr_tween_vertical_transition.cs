﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_tween_vertical_transition : MonoBehaviour
{
    [SerializeField] float vectorY_goal;
    [SerializeField] Vector2 default_location;
    [SerializeField] float duration;
    [SerializeField] GameObject tween_object;
    [SerializeField] scr_audio_manager_2 ref_audio_manager;
    [SerializeField] scr_card_manager ref_card_manager;
    [SerializeField] scr_info_button_function ref_info_button;
    void Awake()
    {
        tween_object = this.gameObject;
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager_2>();
    }

    void OnEnable()
    {
        StartCoroutine(Tween_Move());
    }

    public IEnumerator Tween_Move()
    {   
        yield return new WaitForSeconds(1.5f);
        LeanTween.moveLocalY(tween_object,vectorY_goal,duration).setOnComplete(Default_Location);
        ref_audio_manager.Play_Audio("swoosh_blue_screen_sfx");
        yield return new WaitForSeconds(0.75f);
        Set_Card_Button_Active();
    }

    void Set_Card_Button_Active()
    {
        ref_info_button.Prop_How_to_Play_button_object.GetComponent<Button>().enabled = true;
        ref_info_button.Prop_Clue_button_object.GetComponent<Button>().enabled = true;
        ref_info_button.Prop_Teacher_button_object.GetComponent<Button>().enabled = true;

        for(int i = 0; i < ref_card_manager.Prop_Array_Cards.Length;i++)
        {
            ref_card_manager.Prop_Array_Cards[i].GetComponent<Button>().enabled = true;
        }
    }

    void Default_Location()
    {
        tween_object.GetComponent<RectTransform>().anchoredPosition = default_location;
        tween_object.SetActive(false);
    }

}
