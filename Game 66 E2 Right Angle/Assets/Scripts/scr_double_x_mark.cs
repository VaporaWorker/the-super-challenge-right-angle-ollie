﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_double_x_mark : MonoBehaviour
{
    [SerializeField]List<GameObject> x_dog_mark;
    [SerializeField]float x_mark_index;
    public List<GameObject> Prop_X_Dog_Mark
    {
        get{return x_dog_mark;}
        set{x_dog_mark = value;}
    }
}
