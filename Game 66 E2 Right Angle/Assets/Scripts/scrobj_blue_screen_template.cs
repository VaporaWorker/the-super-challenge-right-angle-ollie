﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Question")]
public class scrobj_blue_screen_template : ScriptableObject
{
   public Sprite question_card;
   public Sprite[] angle_choice_cards;     

}
